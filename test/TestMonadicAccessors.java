import monads.Failure;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * User: aralvarez
 * Date: 26/09/13
 */
public class TestMonadicAccessors {

    private A chainWithoutNulls;
    private A chainWithBNull;

    private Result result;
    private SetterM<Result, String> setResult;
    private GetterM<A, B>           getterBFromA;
    private GetterM<B, C>           getterCFromB;
    private GetterM<C, String>      getterDFromC;

    class A{
        private B b;

        A(B b) {
            this.b = b;
        }

        B getB() {
            return b;
        }

        void setB(B b) {
            this.b = b;
        }
    }

    class B{
        private C c;

        B(C c) {
            this.c = c;
        }

        C getC() {
            return c;
        }

        void setC(C c) {
            this.c = c;
        }
    }

    class C{
        private String d;

        C(String d) {
            this.d = d;
        }

        String getD() {
            return d;
        }

        void setD(String d) {
            this.d = d;
        }
    }

    class Result{
        private String s;

        String getS() {
            return s;
        }

        void setS(String s) {
            this.s = s;
        }
    }

    @Before
    public void setUp(){
        chainWithoutNulls = new A(new B(new C("value")));
        chainWithBNull = new A(null);
        result = new Result();

        setResult = new SetterM<Result, String>(result) {
            protected void set(Result f, String str) {
                f.setS(str);
            }
        };

        getterBFromA = new GetterM<A, B>() {
            protected B get(A a) {
                return a.getB();
            }
        };

        getterCFromB = new GetterM<B, C>() {
            protected C get(B b) {
                return b.getC();
            }
        };

        getterDFromC= new GetterM<C, String>() {
            protected String get(C c) {
                return c.getD();
            }
        };
    }

    @Test
    public void gettersChainWorksWithNoNulls(){

        try {
            getterBFromA.apply(chainWithoutNulls).bind(getterCFromB).bind(getterDFromC).bind(setResult);
        } catch (Failure failure) {
            throw new RuntimeException("You are a failure at functional programming");
        }

        assertEquals("value", result.getS());
    }

    @Test
    public void setterSetsNullIfSomeElementInTheChainIsNull(){

        try {
            getterBFromA.apply(chainWithBNull).bind(getterCFromB).bind(getterDFromC).bind(setResult);
        } catch (Failure failure) {
            throw new RuntimeException("You are a failure at functional programming");
        }

        assertEquals(null, result.getS());
    }
}
