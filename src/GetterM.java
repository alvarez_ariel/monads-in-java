import monads.Applicable;
import monads.Failure;
import monads.Maybe;
import monads.Monad;

/**
 * User: aralvarez
 * Date: 26/09/13
 */
public abstract class GetterM<Container, Returned>
        implements Applicable<Container, Monad<Maybe, Returned>> {


    public Monad<Maybe, Returned> apply(Container c) throws Failure {
        Maybe<Returned> ans;
        Returned result = get(c);

        if(result == null) {
            ans = (Maybe<Returned>)Maybe.NOTHING;
        }else{
            ans = Maybe.pure(result);
        }

        return ans;
    }



    protected abstract Returned get(Container c);
}
