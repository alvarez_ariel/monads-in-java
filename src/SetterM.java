import monads.Applicable;
import monads.Failure;
import monads.Maybe;
import monads.Monad;

/**
 * User: aralvarez
 * Date: 26/09/13
 */
public abstract class SetterM<Receiver, Datum> implements Applicable<Datum, Monad<Maybe, Receiver>> {
    protected SetterM(Receiver r) {
        this.receiver = r;
    }

    // a monadic set function
    public final Monad<Maybe, Receiver> apply(Datum d) throws Failure {
        set(receiver, d);
        return Maybe.pure(receiver);
    }

    // subclasses implement with the particular set method being called
    protected abstract void set(Receiver r, Datum d);

    private final Receiver receiver;
}