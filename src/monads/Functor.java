package monads;

/**
 * User: aralvarez
 * Date: 26/09/13
 */
public interface Functor <F, T> {
    public <T1> Applicable< ? extends Functor<F, T>, ? extends Functor<F, T1> >
        fmap(Applicable<T, T1> f) throws Failure;


    public T arg() throws Failure;
}