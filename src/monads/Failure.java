package monads;

/**
 * User: aralvarez
 * Date: 26/09/13
 */
public class Failure extends Exception {
    public Failure(String string) {
        super(string);
    }
}
