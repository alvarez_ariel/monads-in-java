package monads;

/**
 * User: aralvarez
 * Date: 26/09/13
 */
public interface Applicable<T1, T2> {
    public T2 apply(T1 arg) throws Failure;
}
