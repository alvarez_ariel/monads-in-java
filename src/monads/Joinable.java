package monads;

/**
 * User: aralvarez
 * Date: 26/09/13
 */
public interface Joinable<F, T> extends Functor<F, T> {
    public Functor<F, ?> join() throws Failure;
}